---
id: release-sepiasearch
title: Voici Sepia Search !
date: September 22, 2020
---

<p>Bonjour à toutes et à tous,</p>
<p>Nous venons de publier <a target="_blank" href="https://sepiasearch.org">Sepia
    Search</a>, notre moteur de recherche pour vous faire découvrir des vidéos et des chaînes sur PeerTube !</p>
<p>Nous avons œuvré pour s'assurer que ce moteur respecte vos données, votre attention et
  vos libertés.</p>
<ul>
  <li>Lire les détails <a target="_blank"
      href="https://framablog.org/2020/09/22/sepia-search-our-search-engine-to-promote-peertube/">sur le Framablog</a>
  </li>
  <li>Découvrir <a target="_blank" href="https://sepiasearch.org">Sepia Search</a></li>
  <li>Copier et adapter <a target="_blank" href="https://framagit.org/framasoft/peertube/search-index">le code</a>, pour
    faire votre propre moteur de
    recherche PeerTube.</li>
</ul>
<a target="_blank" href="https://sepiasearch.org"><img loading="lazy"
      src="/img/news/release-sepiasearch/fr/sepiasearchbar.png" alt=""></a>
<p>Pensez à partager <a target="_blank" href="https://joinpeertube.org/roadmap/">la page
    de
    le feuille de route vers la v3</a>, où l'on peut découvrir (et soutenir) nos projets pour PeerTube.</p>
<p><span>Merci à tous les contributeurs de PeerTube !</span><br> Framasoft </p>
