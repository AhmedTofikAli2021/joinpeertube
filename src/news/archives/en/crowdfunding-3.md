---
id: crowdfunding-3
title: 'PeerTube crowdfunding newsletter #3'
date: 'September 12, 2018'
---

Hello everyone!

A month before the version 1 of PeerTube, we would like to share some (good!) news with you.

We just released PeerTube _beta 12_, that allows to subscribe to video channels, whether they are on your instance or even on remote instances. This way, you can browse videos of your subscribed channels in a dedicated page. Moreover, if your PeerTube administrator allows it, you can search a channel or a video directly by typing their web address in the PeerTube search bar.

It was not included in the crowdfunding, but we created an "Overview" page, that displays videos of some categories/tags/channels picked randomly, to show the diversity of the videos uploaded on PeerTube. You can see [a demonstration here](https://peertube3.cpy.re/videos/overview).

You can read the complete _beta 12_ changelog [here](https://github.com/Chocobozzz/PeerTube/blob/develop/CHANGELOG.md#v100-beta12).

Regarding the crowdfunding, most of the rewards are ready: [the PeerTube README](https://github.com/Chocobozzz/PeerTube) and [the JoinPeerTube Hall of Fame](https://joinpeertube.org/hall-of-fame) show off the names of the persons who have chosen the corresponding rewards. We will soon be able to send the personalized thank-you digital arts to people that gave 80€ (~93 USD) and more - and it's so beautiful that we are looking forward to it!

The last feature we have to implement is the videos redundancy between instances, which will further increase resilience on instance overload. If all goes well, we should finish it in about two weeks (end of september).

We remind you that you can track the progress of the work directly [on the git repository](https://github.com/Chocobozzz/PeerTube), and be part of the discussions/bug reports/feature requests in the "Issues" tab.

Moreover, you can ask questions on [the PeerTube forum](https://framacolibri.org/c/qualite/peertube). You can also contact us directly on https://contact.framasoft.org.

Cheers,
Framasoft
